#!/usr/bin/env python3

from slurmy import JobHandler, SingularityWrapper, Slurm
import os
import sys
import json

arg_dict = dict(
    path = "/home/n/Nikolai.Hartmann/bin",
    rucio_account = "nihartma",
    voms_path = "/home/n/Nikolai.Hartmann/x509up_u12255",
    singularity_img = "/project/etp3/ThomasMaier/Singularity/cern-slc6.img",
    #tmpdir = "/tmp/ri23yub",
    #code_path = "/home/grid/lcg/home/ri23yub/project/SusySkim1LInclusive_submodules",
    output_file = "DAOD_EXOT12.test.pool.root",
    target_dir = "@SLURMY.output_dir"
)

def job_script(input_file, arg_dict, jobname):

    arg_dict = arg_dict.copy()

    script = """

    echo "################  Starting on host " `hostname -f` " at  " `date`

    export PATH={path}:$PATH

    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source ${{ATLAS_LOCAL_ROOT_BASE}}/user/atlasLocalSetup.sh

    export RUCIO_ACCOUNT={rucio_account}
    export X509_USER_PROXY={voms_path}
    lsetup rucio

    echo "Work dir: "$TEMPDIR

    pushd $TEMPDIR

    asetup AthDerivation,21.2.33.0,here

    export XRD_LOGLEVEL=Debug
    export XRD_LOGFILE=xrd_log.txt

    cat > args.json<<EOF
    {{
    "preExec": {{
        "all": [
            "rec.doApplyAODFix.set_Value_and_Lock(True);from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \\"BTagCalibRUN12Onl-08-40\\"; from AthenaCommon.AlgSequence import AlgSequence; topSequence = AlgSequence(); topSequence += CfgMgr.xAODMaker__DynVarFixerAlg( \\"InDetTrackParticlesFixer\\", Containers = [ \\"InDetTrackParticlesAux.\\" ] );import AthenaPoolCnvSvc.AthenaPool; svcMgr.AthenaPoolCnvSvc.InputPoolAttributes += [ \\"DatabaseName = '*'; ContainerName = 'CollectionTree'; TREE_CACHE = '-1'\\" ]"
        ]
    }}
    }}
    EOF

    echo "############### args.json ####################"
    cat args.json
    echo ""

    /usr/bin/time wrap_nw2.sh log_nw2.txt Reco_tf.py  --inputAODFile="{input_file}" --outputDAODFile test.pool.root --reductionConf EXOT12 --argjson args.json --maxEvents 500

    code_exit=$?
    echo "Reco_tf.py exit code: ${{code_exit}}"

    # echo "################ NW2 log #################"
    # cat log_nw2.txt
    # echo ""

    cp log_nw2.txt @SLURMY.output_dir/{jobname}_nw2.txt

    # ls {output_file}

    # check root file
    cat <<EOF | python
    import ROOT
    import sys

    f = ROOT.TFile.Open("{output_file}")
    if not f:
        exit(1)
    if len(f.GetListOfKeys()) < 1:
        exit(1)
    if f.IsZombie():
        exit(1)
    exit(0)
    EOF

    res=$?

    cp {output_file} {output_path}

    echo ""
    echo "-----------------XRD DEBUG OUTPUT-----------------"
    cat xrd_log.txt

    popd

    # if code exited non-zero, yield this exit code
    [[ $code_exit -gt 0 ]] && exit $code_exit

    # otherwise the one from the file check
    exit $res
"""


    # remove indention
    lines = script.split("\n")
    script = "\n".join([line[4:] for line in lines])

    arg_dict["output_path"] = os.path.join(arg_dict["target_dir"], input_file.split("/")[-1])
    arg_dict["input_file"] = input_file
    arg_dict["jobname"] = jobname

    return script.format(**arg_dict)

test_sites = [
    'BEIJING-LCG2_DATADISK',
    'CERN-PROD_DATADISK',
    'DESY-HH_DATADISK',
    'DESY-ZN_DATADISK',
    'LRZ-LMU_DATADISK',
    'NDGF-T1_DATADISK',
    'RU-PROTVINO-IHEP_DATADISK',
    'SARA-MATRIX_DATADISK',
    'TOKYO-LCG2_LOCALGROUPDISK',
    'UKI-SCOTGRID-GLASGOW_DATADISK',
]

def get_jh(name):
    jh = JobHandler(
        name = name,
        wrapper = SingularityWrapper(arg_dict["singularity_img"]),
        backend = Slurm(
            export = 'NONE',
            mem = '3500mb',
            #time = '00:30:00'
            time = '02:00:00',
            #exclude = 'gar-ws-etp02,gar-ws-etp80,gar-ws-etp88',
            #mem = '3500mb'
        ),
        #max_retries=2,
        run_max=1,
    )
    return jh

test_directio = True

turl_dict = json.load(open("turls.json"))

if not test_directio:

    for site in test_sites:
        #site = "DESY-HH_DATADISK"

        jh = get_jh('test_xcache_{}'.format(site))

        print ("Proceed with {}?".format(site))
        input()

        for i, file_path in enumerate(turl_dict[site]):
            prefix = "root://lcg-lrz-xcache0.grid.lrz.de:1094"
            first_name = "{}_xcache_first_{}".format(site.replace("-", "_"), i)
            full_path = prefix+"//"+file_path
            # first go - should not be cached (should delete cache before)
            jh.add_job(
                run_script = job_script(
                    full_path,
                    arg_dict,
                    jobname=first_name
                ),
                name = first_name,
                tags = first_name
            )
            # second go - should be cached
            second_name = "{}_xcache_second_{}".format(site.replace("-", "_"), i)
            jh.add_job(
                run_script = job_script(
                    full_path,
                    arg_dict,
                    jobname=second_name
                ),
                name = second_name,
                tags = second_name,
                parent_tags = first_name
            )

        jh.run_jobs()

else:

    jh = get_jh("test_xcache_directio_ttreecache")
    for site in test_sites:
        for  i, file_path in enumerate(turl_dict[site]):
            job_name = "{}_directio_{}".format(site.replace("-", "_"), i)
            jh.add_job(
                run_script = job_script(
                    file_path,
                    arg_dict,
                    jobname=job_name,
                ),
                name = job_name,
            )
    jh.run_jobs()
